/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.entities;

import com.semanticsquare.thrillio.constants.BookGenre;
import com.semanticsquare.thrillio.partner.Shareable;
import java.util.Arrays;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class Book extends Bookmark  implements Shareable {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">

    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    private long id;
    private int publicationYear;
    private String publisher;
    private String[] authors;
    private String genre;
    private double amazonRating;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    public int getPublicationYear() {
        return publicationYear;
    }

    public void setPublicationYear(int publicationYear) {
        this.publicationYear = publicationYear;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public double getAmazonRating() {
        return amazonRating;
    }

    public void setAmazonRating(double amazonRating) {
        this.amazonRating = amazonRating;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Book{" + "id=" + id + ", publicationYear=" + publicationYear 
                + ", publisher=" + publisher + ", authors=" 
                + Arrays.toString(authors) + ", genre=" + genre 
                + ", amazonRating=" + amazonRating + '}';
    }

    @Override
    public String getItemData() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("<item>");
        sb.append("<type>Book</type>");
        sb.append("<title>").append(getTitle()).append("</title>");
        sb.append("<authors>").append(StringUtils.join(authors, ", ")).append("</title>");
        sb.append("<publisher>").append(publisher).append("</publisher>");
        sb.append("<publicationYear>").append(publicationYear).append("</publicationYear>");
        sb.append("<genre>").append(genre).append("</genre>");
        sb.append("<amazonRating>").append(amazonRating).append("</amazonRating>");
        
        sb.append("</item>");
        
        return sb.toString();
    }
    
    @Override
    public boolean isKidFriendlyEligible() {
        if ( genre.equalsIgnoreCase(BookGenre.PHILOSOPHY) 
                || genre.equalsIgnoreCase(BookGenre.SELF_HELP) ) {
            return false;
        }
        
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    //</editor-fold>

}
