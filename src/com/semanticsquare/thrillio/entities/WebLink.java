/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.semanticsquare.thrillio.entities;

import com.semanticsquare.thrillio.partner.Shareable;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class WebLink extends Bookmark implements Shareable {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    private String url;
    private String host;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    @Override
    public String toString() {
        return "WebLink{" + "url=" + url + ", host=" + host + '}';
    }

    @Override
    public String getItemData() {
        StringBuilder sb = new StringBuilder();
        
        sb.append("<item>");
        sb.append("<type>WebLink</type>");
        sb.append("<title>").append(getTitle()).append("</title>");
        sb.append("<url>").append(url).append("</url>");
        sb.append("<host>").append(host).append("</host>");
        sb.append("</item>");
        
        return sb.toString();
    }

    @Override
    public boolean isKidFriendlyEligible() {
        if ( url.toLowerCase().contains("porn") 
                || getTitle().toLowerCase().contains("porn")
                || host.toLowerCase().contains("adult") ) {
            return false;
        }
        
        return true;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    
    //</editor-fold>


}
