/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.partner;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public interface Shareable {
    String getItemData();
}
