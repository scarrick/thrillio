/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.dao;

import com.semanticsquare.thrillio.DataStore;
import com.semanticsquare.thrillio.entities.User;

/**
 *
 * @author Sean Carrick
 */
public class UserDao {
    public User[] getUsers() {
        return DataStore.getUsers();
    }
}
