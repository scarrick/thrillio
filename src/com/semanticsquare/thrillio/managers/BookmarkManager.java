/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.semanticsquare.thrillio.managers;

import com.semanticsquare.thrillio.dao.BookmarkDao;
import com.semanticsquare.thrillio.entities.Book;
import com.semanticsquare.thrillio.entities.Bookmark;
import com.semanticsquare.thrillio.entities.Movie;
import com.semanticsquare.thrillio.entities.User;
import com.semanticsquare.thrillio.entities.UserBookmark;
import com.semanticsquare.thrillio.entities.WebLink;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class BookmarkManager {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    private static BookmarkManager instance = new BookmarkManager();
    private static BookmarkDao dao = new BookmarkDao();
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    public static BookmarkManager getInstance() {
        return instance;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    private BookmarkManager() {
        
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    public Movie createMovie(long id, String title, String profileUrl, 
            int releaseYear, String[] cast, String[] directors, String genre, 
            double imdbRating) {
    Movie movie = new Movie();
    
    movie.setId(id);
    movie.setCast(cast);
    movie.setDirectors(directors);
    movie.setGenre(genre);
    movie.setImdbRating(imdbRating);
    movie.setProfileUrl(profileUrl);
    movie.setReleaseYear(releaseYear);
    movie.setTitle(title);
    
    return movie;
    }
    
    public Book createBook(long id, int publicationYear, String publisher, 
            String title, String[] authors, String genre, double amazonRating) {
        Book book = new Book();
        
        book.setAmazonRating(amazonRating);
        book.setAuthors(authors);
        book.setGenre(genre);
        book.setId(id);
        book.setPublicationYear(publicationYear);
        book.setPublisher(publisher);
        book.setTitle(title);
        
        return book;
    }
    
    public WebLink createWebLink(long id, String title, String url, String host) {
        WebLink webLink = new WebLink();
        
        webLink.setId(id);
        webLink.setUrl(url);
        webLink.setHost(host);
        webLink.setTitle(title);
        
        return webLink;
    }
    
    public Bookmark[][] getBookmarks() {
        return dao.getBookmarks();
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    
    //</editor-fold>

    public void saveUserBookmark(User user, Bookmark bookmark) {
        UserBookmark userBookmark = new UserBookmark();
        userBookmark.setUser(user);
        userBookmark.setBookmark(bookmark);
        
        dao.saveUserBookmark(userBookmark);
    }

    public void setKidFriendlyStatus(User user, String kidFriendlyStatus, 
            Bookmark bookmark) {
        bookmark.setKidFriendlyStatus(kidFriendlyStatus);
        bookmark.setKidFriendlyMarkedBy(user);
        System.out.println("Kid-Friendly status: " 
                + kidFriendlyStatus + ", Marked by: " 
                + user.getEmail() + ", " + bookmark);
    }

    public void share(User user, Bookmark bookmark) {
        bookmark.setSharedBy(user);
        System.out.println("Data to be shared: ");
        if ( bookmark instanceof Book ) {
            System.out.println(((Book)bookmark).getItemData());
        } else if ( bookmark instanceof WebLink ) {
            System.out.println(((WebLink)bookmark).getItemData());
        }
    }

}
