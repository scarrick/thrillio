/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.managers;

import com.semanticsquare.thrillio.dao.UserDao;
import com.semanticsquare.thrillio.entities.User;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class UserManager {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">

    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    private static UserDao dao = new UserDao();
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    private static UserManager instance = new UserManager();
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    public static UserManager getInstance() {
        return instance;
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Constructors">
    private UserManager() {

    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    public User createUser(long id, String email, String password, 
            String firstName, String lastName, int gender, String userType) {
        User user = new User();
        user.setId(id);
        user.setEmail(email);
        user.setFirstName(firstName);
        user.setGender(gender);
        user.setLastName(lastName);
        user.setType(userType);
        user.setPassword(password);
        
        return user;
    }
    
    public User[] getUsers() {
        return dao.getUsers();
    }
    //</editor-fold>
    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    //</editor-fold>
}
