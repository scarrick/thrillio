/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.semanticsquare.thrillio;

import com.semanticsquare.thrillio.entities.Bookmark;
import com.semanticsquare.thrillio.entities.User;
import com.semanticsquare.thrillio.managers.BookmarkManager;
import com.semanticsquare.thrillio.managers.UserManager;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class Thrillio {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    private static User[] users;
    private static Bookmark[][] bookmarks;
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        loadData();
        
        users = UserManager.getInstance().getUsers();
        bookmarks = BookmarkManager.getInstance().getBookmarks();
        
        System.out.println("Printing data ...");
        
        start();
    }

    //</editor-fold>

    private static void loadData() {
        System.out.println("1. Loading data ...");
        DataStore.loadData();
        
        users = DataStore.getUsers();
        bookmarks = DataStore.getBookmarks();
        
//        printUserData();
//        printBookmarkData();
    }

    private static void printUserData() {
        for ( User user : users ) {
            System.out.println(user);
        }
    }

    private static void printBookmarkData() {
        for ( Bookmark[] bookmarkList : bookmarks ) {
            for ( Bookmark bookmark : bookmarkList ) {
                System.out.println(bookmark);
            }
        }
    }

    private static void start() {
//        System.out.println("\n2. Bookmarking ...");
        
        for ( User user : users ) {
            View.browse(user, bookmarks);
        }
    }

}
