/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.semanticsquare.thrillio.constants;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class BookGenre {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">
    public static final String ART = "Art";
    public static final String BIOGRAPHY = "Biography";
    public static final String CHILDREN = "Children";
    public static final String FICTION = "Fiction";
    public static final String HISTORY = "History";
    public static final String MYSTERY = "Mystery";
    public static final String PHILOSOPHY = "Philosophy";
    public static final String RELIGION = "Religion";
    public static final String ROMANCE = "Romance";
    public static final String SELF_HELP = "Self help";
    public static final String TECHNICAL = "Technical";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    private BookGenre() {
        
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    
    //</editor-fold>

}
