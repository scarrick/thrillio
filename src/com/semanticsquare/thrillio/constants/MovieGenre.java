/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.semanticsquare.thrillio.constants;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class MovieGenre {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">
    public static final String CLASSICS = "Classics";
    public static final String DRAMA = "Drama";
    public static final String ACTION = "Action";
    public static final String ACTION_COMEDY = "Action-Comdey";
    public static final String ROMANTIC_MOVIES = "Romantic Movies";
    public static final String ROMANTIC_COMEDY = "Romantic-Comedy";
    public static final String COMEDY = "Comedy";
    public static final String THRILLERS = "Thrillers";
    public static final String CHILDREN_AND_FAMILY = "Children & Family";
    public static final String HORROR = "Horror";
    public static final String SCIFI_AND_FANTASY = "Sci-Fi & Fantasy";
    public static final String MUSIC_AND_MUSICALS = "Music & Musicals";
    public static final String TELEVISION = "Television";
    public static final String SPECIAL_INTEREST = "Special Interest";
    public static final String INDEPENDENT = "Independent";
    public static final String SPORTS_AND_FITNESS = "Sports & Fitness";
    public static final String ANIME_AND_ANIMATION = "Anime & Animation";
    public static final String GAY_AND_LESBIAN = "Gay & Lesbian";
    public static final String CLASSIC_MOVIE_MUSICALS = "Classic Movie Musicals";
    public static final String FAITH_AND_SPIRITUALITY = "Faith & Spirituality";
    public static final String FOREIGN_DRAMAS = "Foreign Dramas";
    public static final String FOREIGN_ACTION_AND_ADVENTURE = "Foreign Action & Adventure";
    public static final String FOREIGN_THRILLERS = "Foreign Thrillers";
    public static final String TV_SHOWS = "TV Shows";
    public static final String FOREIGN_MOVIES = "Foreign Movies";
    public static final String DOCUMENTARIES = "DOCUMENTARIES";
   
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    private MovieGenre() {
        
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    
    //</editor-fold>

}
