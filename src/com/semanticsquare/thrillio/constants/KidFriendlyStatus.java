/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.semanticsquare.thrillio.constants;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class KidFriendlyStatus {
    //<editor-fold defaultstate="collapsed" desc="Static Constants">
    public static final String APPROVED = "approved";
    public static final String REJECTED = "rejected";
    public static final String UNKNOWN = "unknown";
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Variables">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Member Fields">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Static Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Initializers">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Constructors">
    private KidFriendlyStatus() {
        
    }
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Instance Nested Classes">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Public Instance Methods">
    
    //</editor-fold>

    //<editor-fold defaultstate="collapsed" desc="Private Instance Methods">
    
    //</editor-fold>

}
