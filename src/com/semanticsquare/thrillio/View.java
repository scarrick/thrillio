/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio;

import com.semanticsquare.thrillio.constants.KidFriendlyStatus;
import com.semanticsquare.thrillio.constants.UserType;
import com.semanticsquare.thrillio.controllers.BookmarkController;
import com.semanticsquare.thrillio.entities.Bookmark;
import com.semanticsquare.thrillio.entities.User;
import com.semanticsquare.thrillio.partner.Shareable;

/**
 *
 * @author Sean Carrick
 */
public class View {
    public static void browse(User user, Bookmark[][] bookmarks) {
        System.out.println("\n" + user + " is browsing items ...");
        int bookmarkCount = 0;
        
        for ( Bookmark[] bookmarkList : bookmarks ) {
            for ( Bookmark bookmark : bookmarkList ) {
                if ( bookmarkCount < DataStore.USER_BOOKMARK_LIMIT ) {
                    boolean isBookmarked = getBookmarkDecision(bookmark);
                    if ( isBookmarked) {
                        bookmarkCount++;
                        
                        BookmarkController.getInstance().saveUserBookmark(user, 
                                bookmark);
            
                        System.out.println("New Item Bookmarked: " +bookmark);
                    }
                }
                    
                if ( user.getType().equals(UserType.EDITOR)
                        || user.getType().equals(UserType.CHIEF_EDITOR)) {
                    
                    // Mark as kid-friendly
                    if ( bookmark.isKidFriendlyEligible() 
                            && bookmark.getKidFriendlyStatus().equals(
                                    KidFriendlyStatus.UNKNOWN) ) {
                        String kidFriendlyStatus = getKidFriendlyStatusDecision(
                                bookmark);
                        
                        if ( !kidFriendlyStatus.equalsIgnoreCase(
                                KidFriendlyStatus.UNKNOWN) ) {
                            BookmarkController.getInstance()
                                    .setKidFriendlyStatus(user, kidFriendlyStatus, bookmark);
                            
                        }
                    }
                    
                    // Sharing.
                    if ( bookmark.getKidFriendlyStatus().equalsIgnoreCase(
                            KidFriendlyStatus.APPROVED) && 
                            bookmark instanceof Shareable ) {
                        boolean isShared = getShareDecision();
                        
                        if ( isShared ) {
                            BookmarkController.getInstance().share(user, bookmark);
                        }
                    }
                }
            }
            
            
        }
        
        /*
        for ( int i = 0; i < DataStore.USER_BOOKMARK_LIMIT ; i++ ) {
            int typeOffset = (int) (Math.random() 
                    * DataStore.BOOKMARK_TYPES_COUNT);
            int bookmarkOffset = (int) (Math.random() 
                    * DataStore.BOOKMARK_COUNT_PER_TYPE);
            
            Bookmark bookmark = bookmarks[typeOffset][bookmarkOffset];
            
            BookmarkController.getInstance().saveUserBookmark(user, bookmark);
            
            System.out.println(bookmark);
        }*/
    }

    // TODO Below methods simulate user input. After IO, we take input via console.
    private static boolean getBookmarkDecision(Bookmark bookmark) {
        // Hack: Simulating user input...
        return Math.random() > 0.5 ? true : false;
    }

    private static String getKidFriendlyStatusDecision(Bookmark bookmark) {
        // Hack: Simulating user input...
        return Math.random() < 0.4 ? KidFriendlyStatus.APPROVED :
                (Math.random() >= 0.4 && Math.random() < 0.8) ? 
                KidFriendlyStatus.REJECTED : KidFriendlyStatus.UNKNOWN;
    }

    private static boolean getShareDecision() {
        // Hack: Simulating user input...
        return Math.random() > 0.5 ? true : false;
    }
}
