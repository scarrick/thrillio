/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.entities;

import com.semanticsquare.thrillio.constants.MovieGenre;
import com.semanticsquare.thrillio.managers.BookmarkManager;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class MovieTest {
    
    public MovieTest() {
    }

    /**
     * Test of isKidFriendlyEligible method, of class Movie.
     */
    @Test
    public void testIsKidFriendlyEligible() {
        // Test 1: Horror movies are not kid friendly.
        Movie movie = BookmarkManager.getInstance().createMovie(3000L, 
                "Citizen Kane", "", 1941, 
                new String[] {"Orson Welles"," Joseph Cotten"},  
                    new String[] {"Orson Welles"}, MovieGenre.HORROR, 8.5);
        
        boolean isKidFriendlyEligible = movie.isKidFriendlyEligible();
        
        assertFalse("For Horror Genre - isKidFriendlyEligible should return "
                + "false", isKidFriendlyEligible);

        // Test 2: Thriller movies are not kid friendly.
        movie = BookmarkManager.getInstance().createMovie(3000L, 
                "Citizen Kane", "", 1941, 
                new String[] {"Orson Welles"," Joseph Cotten"},  
                    new String[] {"Orson Welles"}, MovieGenre.THRILLERS, 8.5);
        
        isKidFriendlyEligible = movie.isKidFriendlyEligible();
        
        assertFalse("For Thrillers Genre - isKidFriendlyEligible should return "
                + "false", isKidFriendlyEligible);
    }
    
}
