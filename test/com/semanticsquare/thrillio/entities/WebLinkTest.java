/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.entities;

import com.semanticsquare.thrillio.managers.BookmarkManager;
import static junit.framework.Assert.*;
import org.junit.Test;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class WebLinkTest {
    
    public WebLinkTest() {
    }

    public static void setUpClass() throws Exception {
    }

    public static void tearDownClass() throws Exception {
    }

    public void setUp() throws Exception {
    }

    public void tearDown() throws Exception {
    }

    /**
     * Test of isKidFriendlyEligible method, of class WebLink.
     */
    @Test
    public void testIsKidFriendlyEligible() {
        // Test 1: porn in url -- false
        WebLink webLink = BookmarkManager.getInstance().createWebLink(2000, 
                "Taming Tiger, Part 2", "http://www.javaworld.com/article/20727"
                        + "59/core-java/taming-porn--part-2.html", 
                "http://www.javaworld.com");
        
        boolean isKidFriendlyEligible = webLink.isKidFriendlyEligible();
        
        assertFalse("For porn in url - isKidFriendlyEligible() must return "
                + "false", isKidFriendlyEligible);
        
        // Test 2: porn in title -- false
        webLink = BookmarkManager.getInstance().createWebLink(2000, 
                "Taming Porn, Part 2", "http://www.javaworld.com/article/20727"
                        + "59/core-java/taming-tiger--part-2.html", 
                "http://www.javaworld.com");
        
        isKidFriendlyEligible = webLink.isKidFriendlyEligible();
        
        assertFalse("For porn in title - isKidFriendlyEligible() must return "
                + "false", isKidFriendlyEligible);
        
        // Test 3: adult in host -- false
        webLink = BookmarkManager.getInstance().createWebLink(2000, 
                "Taming Tiger, Part 2", "http://www.javaworld.com/article/20727"
                        + "59/core-java/taming-tiger--part-2.html", 
                "http://www.adult.com");
        
        isKidFriendlyEligible = webLink.isKidFriendlyEligible();
        
        assertFalse("For adult in host - isKidFriendlyEligible() must return "
                + "false", isKidFriendlyEligible);
        
        // Test 4: adult in url, but not in host -- true
        webLink = BookmarkManager.getInstance().createWebLink(2000, 
                "Taming Tiger, Part 2", "http://www.javaworld.com/article/20727"
                        + "59/core-java/taming-adult--part-2.html", 
                "http://www.javaworld.com");
        
        isKidFriendlyEligible = webLink.isKidFriendlyEligible();
        
        assertTrue("For adult in url but not host - isKidFriendlyEligible() must return "
                + "true", isKidFriendlyEligible);
        
        // Test 5: adult in title only -- true
        webLink = BookmarkManager.getInstance().createWebLink(2000, 
                "Taming Adult, Part 2", "http://www.javaworld.com/article/20727"
                        + "59/core-java/taming-tiger--part-2.html", 
                "http://www.javaworld.com");
        
        isKidFriendlyEligible = webLink.isKidFriendlyEligible();
        
        assertTrue("For adult in title - isKidFriendlyEligible() must return "
                + "true", isKidFriendlyEligible);
    }
    
}
