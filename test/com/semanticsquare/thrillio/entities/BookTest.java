/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.semanticsquare.thrillio.entities;

import com.semanticsquare.thrillio.constants.BookGenre;
import com.semanticsquare.thrillio.managers.BookmarkManager;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick <sean at carricktrucking.com>
 */
public class BookTest {
    
    public BookTest() {
    }

    /**
     * Test of isKidFriendlyEligible method, of class Book.
     */
    @Test
    public void testIsKidFriendlyEligible() {
        // Test 1: Philosphy Genre -- false
        Book book = BookmarkManager.getInstance().createBook(4000L, 1854, 
                "Porn on Walden", "Wilder Publications", 
                new String[] {"Henry David Thoreau"}, 
                BookGenre.PHILOSOPHY, 4.3);
        
        boolean isKidFriendlyEligible = book.isKidFriendlyEligible();
        
        assertFalse("For Philosophy Genre - isKidFriendlyEligible() must return "
                + "false", isKidFriendlyEligible);
        
        // Test 2: Self Help Genre -- false
        book = BookmarkManager.getInstance().createBook(4000L, 1854, 
                "Walden", "Wilder Publications", 
                new String[] {"Henry David Thoreau"}, 
                BookGenre.SELF_HELP, 4.3);
        
        isKidFriendlyEligible = book.isKidFriendlyEligible();
        
        assertFalse("For Self-Help Genre - isKidFriendlyEligible() must return "
                + "false", isKidFriendlyEligible);
    }
    
}
